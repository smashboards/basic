// vim: set softtabstop=2 ts=2 sw=2 expandtab:
// strict mode
"use strict";

/**
* Convert timestamp from Postgres into locale-aware format.
*/
function localTimeStamp(ts) {
  var d = new Date(ts);
  return(d.toLocaleString());
}


/**
* TODO: implement
*/
function i18n(s1, ...vars) {
  return(s1 + "; " + vars.join(", "));
}


/**
* Error message
*/
function error(text) {
  console.error("ERROR: " + text);
}


