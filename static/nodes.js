// vim: set softtabstop=2 ts=2 sw=2 expandtab:
// strict mode
"use strict";

const States = ['okay', 'unknown', 'warning', 'error', 'unusable'];

class Node {
  /* Class design parameters:
  ** - Don't store data which is already in the DOM.  Use the DOM
  ** - Ignore the above if it means such as linearly searching through the DOM
  **   for stuff
  */
  static container = null;
  static lookup;
  static {
    this.lookup = new Map();
  }

  static configure(container) {
    this.container = container;
  }

  static have_problems() {
    for (const [name, node] of this.lookup) {
      if (node.dom.card.dataset.problems > 0) {
        return true;
      }
    }
    return false;
  }

  static expand_all() {
    for (const [name, node] of this.lookup) {
      node.show_full();
    }
  }

  static expand_problems() {
    for (const [name, node] of this.lookup) {
      node.show_problems();
    }
  }

  static collapse_all() {
    for (const [name, node] of this.lookup) {
      node.show_summary();
    }
  }

  //static populate(name, info) {
  //  lookup.get(name).populate(info);
  //}

  constructor(container, spec) {
    this.name = spec.node;
    Node.lookup.set(spec.node, this);

    // initial HTML content is the same for both full and summary views
    const initHTML = `<caption title='Registered ${localTimeStamp(spec.registration)}'>${spec.node}</caption>`;

    // create main node card
    const card = document.createElement("div");
    card.id = `node_${this.name}`;
    card.className = 'node';
    container.appendChild(card);

    // create full info table
    const full = document.createElement("table");
    full.id = `node_full_${this.name}`;
    full.className = 'node_table';
    full.innerHTML = initHTML;
    card.appendChild(full);

    // create summary table
    const summary = document.createElement("table");
    summary.id = `node_summary_${this.name}`;
    summary.className = 'node_summary';
    summary.innerHTML = initHTML;
    card.appendChild(summary);

    // set starting display
    full.style.display = 'none';
    summary.style.display = '';

    // register event handler to switch between full/summary display
    card.addEventListener('click', function(event) {
      if (full.style.display == 'none') {
        full.style.display = '';
        summary.style.display = 'none';
      } else {
        full.style.display = 'none';
        summary.style.display = '';
      }
    });

    // retain DOM elements
    this.dom = {
      card: card,
      full: full,
      summary: summary
    }
  }

  show_full() {
    this.dom.full.style.display = '';
    this.dom.summary.style.display = 'none';
  }

  show_problems() {
    /* Show full display if node has problems; in future this may expand only
    ** the problems, somehow.
    */
    if (this.dom.card.dataset.problems > 0) {
      this.show_full();
    } else {
      this.show_summary();
    }
  }

  show_summary() {
    this.dom.full.style.display = 'none';
    this.dom.summary.style.display = '';
  }

  populate(info) {
    // populate full table
    for (const v of info) {
      // create row for status in node table
      var nodeRow = document.createElement('tr');
      nodeRow.id = `n_${this.name}_s_${v.test}`;
      this.dom.full.appendChild(nodeRow);

      // populate status row
      this.updateStatusRow(nodeRow, v);
    }

    // create (blank) summary table
    var nodeSummaryRow = document.createElement('tr');
    this.dom.summary.appendChild(nodeSummaryRow);
    this.dom.summary_counts = nodeSummaryRow;
    for (var i=0; i<States.length; i++) {
      var el = document.createElement('td');
      nodeSummaryRow.appendChild(el);
      el.innerText = "-";
    }

    // populate summary table
    this.revalSummary();
  }

  revalSummary() {
    var stateTotals = [0, 0, 0, 0, 0];
    var ackStateTotals = [0, 0, 0, 0, 0];

    // iterate through rows in node table
    for (const child of this.dom.full.children) {
      if (child.tagName == "TR") {
        // count statuses in a given state
        stateTotals[States.indexOf(child.dataset.state)] += 1;

        // count acknowledgements for each state
        // TODO: I think I'm setting the below wrong in the first place to be able to compare it this way
        if (child.dataset.acknowledged !== 'undefined') {
          console.log(`acknowledged: ${child.dataset.acknowledged}`);
          ackStateTotals[States.indexOf(child.dataset.state)] += 1;
        }
      }
    }

    // fill out the little boxes
    var problems = 0;
    var i = 0;
    for (const child of this.dom.summary_counts.children) {

      var count = stateTotals[i];
      child.innerText = count;

      // set data-state to the state if value > 0 for non-okay state (because 0
      // errors is "okay")
      if (i > 0 && count > 0) {
        if (ackStateTotals[i] < count) {
          child.dataset.state = States[i];
        } else {
          child.dataset.state = 'acknowledged';
        }
        problems++;
      } else {
        child.dataset.state = 'okay';
      }

      i++;
    };

    // save problem count
    this.dom.card.dataset.problems = problems;
  }

  updateStatus(status) {
    var rowId = `n_${this.name}_s_${status.test}`;
    var row = document.getElementById(rowId);
    this.updateStatusRow(row, status);
    //row.classList.remove('pending');

    // re-evaluate the summary table
    this.revalSummary();
  }

  /**
  * Set details of row in status table
  */
  updateStatusRow(row, status) {
    row.className = `state_${status.state}`;
    row.title = `Reported ${localTimeStamp(status.reported)}`;
    if (status.expected) {
      row.title += `; next expected ${localTimeStamp(status.expected)}`;
    }
    if (status.stale) {
      row.classList.add('stale');
      row.title += '; stale';
    }
    if (status.acknowledged) {
      row.classList.add('acknowledged');
      row.title += `; acknowledged ${localTimeStamp(status.acknowledged)}`;
      if (status.acknowledgement) {
        row.title += ` ("${status.acknowledgement}")`;
      }
    }
    var message = status.message ? status.message.replace(/\n/g, '; ') : '';
    row.innerHTML = `
          <th>${status.test}</th>
          <td>${status.state}</td>
          <td>${message}</td>
  `;

    // set data attributes
    row.dataset.test = status.test;
    row.dataset.state = status.state;
    row.dataset.stale = status.stale;
    row.dataset.acknowledged = status.acknowledged;
  }
}
