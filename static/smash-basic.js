// vim: set softtabstop=2 ts=2 sw=2 expandtab:
// strict mode
"use strict";

/**
* Default refresh period in seconds
*/
const def_refresh = 60;
//const def_refresh = 5;

// globals
var refresh_when_visible = [];
var node_load_message_id = null;
var status_load_message_id = null;
var node_loading = null;
var status_loading = null;

/** counter class
*/
class Counter {
  #count;
  #on_change;

  constructor(initial, on_change) {
    this.#count = initial;
    this.#on_change = on_change;
  }

  get count() {
    return this.#count;
  }

  set count(c) {
    if (this.#count != c) {
      this.#on_change(this.#count, c);
    }
    this.#count = c;
  }
}


/**
* Initialise Smashboard
*/
function runSmashboard() {

  /**
  * Record state of the node view in the expand/collapse toggle, determine and
  * record the anticipated next state, and set the label to the action we'll
  * take to get there if the user presses the button.
  */
  function set_expand_toggle(el, state) {
    const labels = new Map([
      ['full', 'Expand all'],
      ['full-problems', 'Expand problems'],
      ['summary', 'Collapse all']
    ]);
    var next_state;

    switch (state) {
      case 'summary':
        next_state = Node.have_problems ? 'full-problems' : 'full';
        break;
      case 'full-problems':
        next_state = 'full';
        break;
      case 'full':
        next_state = 'summary';
        break;
      default:
        console.error("Undefined expand toggle state: " + state);
        return;
    }

    el.dataset.state = state;
    el.dataset.next = next_state;
    el.textContent = labels.get(next_state);
  }

  /**
  * Handle the expand/collapse toggle button being pressed, honouring the
  * expected transition even if the actual state has changed, which is a slim
  * possibility.  After handling the action, update the button to reflect
  * current state and the next action to take.
  */
  function handle_expand_toggle(event) {
    var el = event.target;

    // take requested action
    switch (el.dataset.next) {
      case 'full':
        Node.expand_all();
        break;
      case 'full-problems':
        Node.expand_problems();
        break;
      case 'summary':
        Node.collapse_all();
        break;
    }

    // presumably that worked so we update the toggle to reflect the new
    // action transitioning the view to the next state
    set_expand_toggle(el, el.dataset.next);
  }

  /**
  * Test if expand toggle is showing correct label for current context state.
  * Specifically, if current state is summary view, set the current state to
  * the same value.
  */
  function reval_expand_toggle(el) {
    if (el.dataset.state == 'summary') {
      set_expand_toggle(el, el.dataset.state);
    }
  }

  // initialize node class
  Node.configure(document.getElementById("nodes"));

  loadNodesGroupedByAttribute("/api", "group");
  // TODO: need a better way to handle this, such as for dev/testing
  // loadNodesGroupedByAttribute("http://localhost:5000/api", "group");

  // initialize expand toggle
  var toggleExpand = document.getElementById("toggle_expand");

  // set default state
  set_expand_toggle(toggleExpand, 'summary');

  // add event listener for toggle
  toggleExpand.addEventListener('click', handle_expand_toggle);

  // create node counter
  node_loading = new Counter(0,
    function(was, willbe) {
      if (willbe == 0) {
        status_clear(node_load_message_id);
        node_load_message_id = null;
        reval_expand_toggle(toggleExpand);
      } else if (was == 0) {
        node_load_message_id = status(`Loading nodes (${willbe})`);
      } else {
        status_update(node_load_message_id, `Loading nodes (${willbe})`);
      }
    }
  );

  // create status counter
  status_loading = new Counter(0,
    function(was, willbe) {
      if (willbe == 0) {
        status_clear(status_load_message_id);
        status_load_message_id = null;
        reval_expand_toggle(toggleExpand);
      } else if (was == 0) {
        status_load_message_id = status(`Loading statuss (${willbe})`);
      } else {
        status_update(status_load_message_id, `Loading statuss (${willbe})`);
      }
    }
  );
}


/**
* Refresh statuses whose refresh timers expired while the application wasn't
* visible, queued in statusUpdateTimerExpired().
*
*/
function refresh_waiting() {
  // remove self from event queue for now
  document.removeEventListener('visibilitychange', refresh_waiting);

  // this should only be executed in a visibility change to visible
  if (document.hidden) {
    // TODO: the logic, and also this alert box should not be here
    alert("Should not be here");
    return;
  }

  // loop through queued-up statii to refresh
  refresh_when_visible.forEach(function(v, i) {
    requestUpdate(v.uri, v.node, v.test);
  });

  // clear the array now the events have been handled
  refresh_when_visible.length = 0;

  // TODO: evaluate the following for avoiding race condition, other issues
  /* I like this better but it doesn't seem to work.  But apart from reading
     better it might avoid a race condition when the list is too long, or
     other potential issues especially when the list is quite long.
    for (var v; v = refresh_when_visible.pop(); ) {
    requestUpdate(v.uri, v.node, v.test);
  }*/
}


/**
* Request list of nodes grouped by attribute
*/
async function loadNodesGroupedByAttribute(base_uri, attribute) {
  var status_id = status(i18n('LOADING_NODES_BY_GROUP'));

  const url = `${base_uri}/attributes/${attribute}/nodes/`;
  var groups;

  try {
    const response = await fetch(url);
    if (!response.ok) {
      throw new Error(`Server response is ${response.status}, ${response.statusText}`);
    }

    groups = await response.json();
  } catch (error) {
    status_clear(status_id);
    console.error("Error in loading groups:", error);
    return;
  }

  // create and populate the node HTML elements
  var nodesDiv = document.getElementById("nodes");
  for (var name in groups) {
    var containerDiv;
    if (name === "") {
      containerDiv = nodesDiv;
    } else {
      containerDiv = layoutGroup(nodesDiv, name);
    }
    groups[name].forEach(function(node, i) {
      var node = new Node(containerDiv, node);
      loadNode(base_uri, node);
    });
  }
  status_clear(status_id);
}


/**
* Create and populate group card
*/
function layoutGroup(targetDiv, name) {
  // create group container and add to target div
  var groupDiv = document.createElement('div');
  groupDiv.classList.add('group');
  groupDiv.id = `group_${name}`;
  targetDiv.appendChild(groupDiv);

  // add content
  groupDiv.innerHTML = `<h1>${name}</h1>`;

  return(groupDiv);
}


/**
* Create and populate node card and request node status
*/
async function loadNode(base_uri, node) {

  // update status message
  node_loading.count++;

  // request node status
  const url = `${base_uri}/nodes/${node.name}/status/`;
  var info;

  try {
    const response = await fetch(url);
    if (!response.ok) {
      throw new Error(`Server response is ${response.status}, ${response.statusText}`);
    }

    info = await response.json();
  } catch (error) {
    node_loading.count--;
    console.error("Error in loading node status:", error);
    return;
  }

  // populate node card
  node.populate(info);

  // set update timers for each status
  for (const status of info) {
    setStatusUpdateTimer(base_uri, node.name, status);
  }

  node_loading.count--;
}


/**
* On timer expiry, request update or defer if inactive
*/
function statusUpdateTimerExpired(uri, nodename, test) {
  if (!document.hidden) {
    requestUpdate(uri, nodename, test);
  } else {

    // add to when-I-awake array
    refresh_when_visible.push({
      'uri': uri,
      'node': nodename,
      'test': test
    });

    // register event listener if not already registered
    document.addEventListener('visibilitychange', refresh_waiting);
  }
}


/**
* Determine and set timer for status refresh
*/
function setStatusUpdateTimer(uri, nodename, status) {
  // check when to expect next update for this test
  var timeout;
  if ('next' in status) {
    timeout = status.next * 1000;
  } else {
    timeout = def_refresh * 1000;
  }

  // create timer for updating status
  setTimeout(function() {
    statusUpdateTimerExpired(uri, nodename, status.test);
  }, timeout);
}


/**
* Request update for given status for given node
*/
async function requestUpdate(base_uri, nodename, test) {
  // update status message
  status_loading.count++;

  // TODO: only do the following if stale; otherwise it's needless flicker
  //var rowId = `n_${nodename}_s_${test}`;
  //var row = document.getElementById(rowId);
  //row.classList.add('pending');

  const url = `${base_uri}/nodes/${nodename}/status/${test}`;
  var info;

  try {
    const response = await fetch(url);
    if (!response.ok) {
      throw new Error(`Server response is ${response.status}, ${response.statusText}`);
    }

    info = await response.json();
  } catch (error) {
    status_loading.count--;
    console.error("Error in requesting updated status:", error);
    return;
  }

  updateStatus(base_uri, nodename, info);
  status_loading.count--;
}


/**
* Invoke updated status for existing node and reset timer
*/
function updateStatus(base_uri, nodename, info) {
  var node = Node.lookup.get(nodename);
  node.updateStatus(info);
  setStatusUpdateTimer(base_uri, nodename, info);
}
