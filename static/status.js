// vim: set softtabstop=2 ts=2 sw=2 expandtab:
/*
** Module for operational status messaging
*/

// strict mode
"use strict";

// globals
var smash_status = "";
var smash_status_id = 0;

/**
* Express a status message
*/
function status(text) {
  var id = ++smash_status_id;

  // create status span child for in status box
  var span = document.createElement('span');
  span.id = `message_${id}`;
  span.className = "status";

  // add as child
  var statusEl = document.getElementById("status");
  statusEl.appendChild(span);

  // set content
  span.textContent = text;

  //console.log(`Status id ${span.id}: ${text}`);
  return(id);
}


function status_update(id, text) {

  // create status span child for in status box
  var span_id = `message_${id}`;
  var span = document.getElementById(span_id);

  // set content
  span.textContent = text;

  //console.log(`Status id ${span.id}: ${text}`);
  return(id);
}


/**
* Clears a status message
*/
function status_clear(id) {
  var span = document.getElementById(`message_${id}`);
  if (span != null) {
    span.remove();
  }
}
